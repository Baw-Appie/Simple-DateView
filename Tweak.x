#import "header.h"

float iosVersion;
BOOL isiOS14 = false;
%ctor {
  iosVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
  if(iosVersion > 14) isiOS14 = true;
}

#define IOS14PADDING (isiOS14 ? 15 : 0)
#define IOS14WEATHERPADDING (isiOS14 ? 10 : 0)

SBUICAPackageView *packageView;

@interface PDDokdo : NSObject
+ (instancetype)sharedInstance;
@property (nonatomic, copy, readonly) NSString *currentTemperature;
@property (nonatomic, copy, readonly) NSString *currentConditions;
@property (nonatomic, copy, readonly) NSString *currentLocation;
@property (nonatomic, strong, readonly) UIImage *currentConditionsImage;
@property(nonatomic, strong, readonly) NSDate *sunrise;
@property(nonatomic, strong, readonly) NSDate *sunset;
@property (nonatomic, strong, readonly) NSDictionary *weatherData;
- (void)refreshWeatherData;
@end

%hook SBLockScreenManager

-(void)_authenticationStateChanged:(NSNotification *)arg1 {
  [packageView setState:[arg1.userInfo[@"SBFUserAuthenticationStateWasAuthenticatedKey"] isEqual:@1] ? @"Locked" : @"Unlocked" animated:true transitionSpeed:1 completion:nil];
  return %orig;
}

%end


%hook SBFLockScreenDateView
%property (nonatomic, assign) BOOL firstRun;
%property (nonatomic, retain) UIImageView *conditionsImageView;
%property (nonatomic, retain) UILabel *temperatureLabel;
%property (nonatomic, retain) SBUICAPackageView *packageView;
-(void)layoutSubviews {
  HBLogError(@"Loaded");
  [[PDDokdo sharedInstance] refreshWeatherData];


  if(!self.firstRun) {

    NSBundle *bundle = [NSBundle bundleForClass:NSClassFromString(@"SBUIProudLockIconView")];
    packageView = [NSClassFromString(@"SBUICAPackageView") alloc];
    [packageView initWithPackageName:@"lock@3x-812h" inBundle:bundle];
    packageView.frame = CGRectMake(self.frame.size.width-64,0,32,32);
    [self addSubview:packageView];

    self.conditionsImageView = [[UIImageView alloc] initWithImage:[[PDDokdo sharedInstance] currentConditionsImage]];
    [self addSubview:self.conditionsImageView];
    self.conditionsImageView.frame = CGRectMake(IOS14WEATHERPADDING,self.frame.size.height-32,32,32);

    self.temperatureLabel = [[UILabel alloc] init];
    [self.temperatureLabel setText:[[PDDokdo sharedInstance] currentTemperature]];
    self.temperatureLabel.frame = CGRectMake(35+IOS14WEATHERPADDING, self.frame.size.height-23, 64, 16);
    [self addSubview:self.temperatureLabel];

    self.firstRun = true;
  }
  self.temperatureLabel.textColor = [[self _timeLabel] textColor];

  if(self.temperatureLabel) self.temperatureLabel.text = [[PDDokdo sharedInstance] currentTemperature];
  if(self.conditionsImageView) self.conditionsImageView.image = [[PDDokdo sharedInstance] currentConditionsImage];


  [self setAlignmentPercent:-1];
  %orig;
}
+(id)timeFont {
  return [UIFont fontWithName:@"Helvetica" size:50];
}

-(void)setDateToTimeStretch:(double)arg1 {
	  %orig(0);
}

%new
-(float)expectedLabelWidth:(SBUILegibilityLabel *)label {
    [label setNumberOfLines:1];
    CGSize expectedLabelSize = [[label string] sizeWithAttributes:@{NSFontAttributeName:label.font}];
    return expectedLabelSize.width + 2;
}

-(CGRect)_timeLabelFrameForAlignmentPercent:(double)arg1 {
	SBUILegibilityLabel *timeLabel = nil;
  object_getInstanceVariable(self, "_timeLabel", (void**)&timeLabel);
  CGRect timing = %orig;
  return CGRectMake (timing.origin.x+IOS14PADDING, timing.origin.y-35, [self expectedLabelWidth:timeLabel], timing.size.height);
}
-(CGRect)_subtitleViewFrameForView:(id)arg1 alignmentPercent:(double)arg2 {
  CGRect timing = %orig;
  return CGRectMake (timing.origin.x+IOS14PADDING, timing.origin.y, timing.size.width, timing.size.height);
}
%end
