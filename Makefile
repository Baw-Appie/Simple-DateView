TARGET = iphone:15.6:15.6
ARCHS = arm64 arm64e

INSTALL_TARGET_PROCESSES = SpringBoard

include $(THEOS)/makefiles/common.mk

TWEAK_NAME = BawAppieDateView

BawAppieDateView_FILES = Tweak.x
BawAppieDateView_LIBRARIES += pddokdo

include $(THEOS_MAKE_PATH)/tweak.mk
