#include <Foundation/Foundation.h>
#include <UIKit/UIKit.h>
#include <HBLog.h>
#import <QuartzCore/QuartzCore.h>
@interface PHInCallUIUtilities : NSObject
+(BOOL)isSpringBoardPasscodeLocked;
+(BOOL)isSpringBoardLocked;
+(long long)interfaceOrientationForDeviceOrientation:(long long)arg1 ;
@end
@interface SBUICAPackageView : UIView
-(id)initWithPackageName:(id)arg1 inBundle:(id)arg2 ;
-(BOOL)setState:(id)arg1 animated:(BOOL)arg2;
-(BOOL)setState:(id)arg1 animated:(BOOL)arg2 transitionSpeed:(double)arg3 completion:(/*^block*/id)arg4 ;
@end
@interface SBLockScreenManager
@property (readonly) BOOL isUILocked;
+(id)sharedInstance;
@end
@interface WeatherPreferences
+(id)sharedPreferences;
@end
@interface WFTemperature
-(double)celsius;
@end
@interface City
@property (assign,nonatomic) long long conditionCode;
@property (nonatomic,retain) WFTemperature * temperature;
@property (copy) id location;
@end
@interface WAForecastModel
@property (nonatomic,retain) City * city;
@end
@interface WATodayAutoupdatingLocationModel : NSObject
-(void)setPreferences:(WeatherPreferences *)arg1 ;
-(WAForecastModel *)forecastModel;
@end
@interface TWCLocationUpdater
+(id)sharedLocationUpdater;
-(void)updateWeatherForLocation:(id)arg1 city:(id)arg2 withCompletionHandler:(/*^block*/id)arg3 ;
-(void)updateWeatherForLocation:(id)arg1 city:(id)arg2 isFromFrameworkClient:(BOOL)arg3 withCompletionHandler:(/*^block*/id)arg4 ;
@end
@interface WeatherImageLoader : NSObject
+ (id)conditionImageWithConditionIndex:(NSInteger)conditionCode;
+ (id)conditionImageNameWithConditionIndex:(NSInteger)conditionCode;
@end

@interface SBUIProudLockIconView : UIView
@end
@interface SBDashBoardView : UIView
@property (nonatomic, retain) UIView *proudLockContainerView;
@end

@interface SBUIBackgroundView : UIView
@end

@interface SiriUIConfiguration : NSObject
-(id)initWithFlamesViewFidelity:(int)arg1 isSystemHostedPresentation:(BOOL)arg2 ;
@end

@interface SiriUISiriStatusView : UIView
-(id)initWithFrame:(CGRect)arg1 screen:(id)arg2 textInputEnabled:(BOOL)arg3 configuration:(id)arg4 ;
-(void)setupOrbIfNeeded;
-(void)_animateSiriGlyphHidden:(BOOL)arg1;
@end

@interface SBAssistantController
+(BOOL)isAssistantVisible;
+(id)sharedInstance;
-(BOOL)handleSiriButtonDownEventFromSource:(NSInteger)arg1 activationEvent:(NSInteger)arg2;
-(void)handleSiriButtonUpEventFromSource:(NSInteger)arg1;
-(void)dismissPluginForEvent:(NSInteger)arg1;
@end

@interface SBFLockScreenDateSubtitleView : UIView
@property (assign,nonatomic) double strength;                                         //@synthesize strength=_strength - In the implementation block
@property (nonatomic,retain) UIFont * font;
@property (nonatomic,readonly) double baselineOffsetFromOrigin;
@property (nonatomic,readonly) double baselineOffsetFromBottom;
@property (nonatomic,retain) NSString * string;
@property (nonatomic,retain) UIView * accessoryView;                                  //@synthesize accessoryView=_accessoryView - In the implementation block
@property (nonatomic,retain) UIView * backgroundView;                                 //@synthesize backgroundView=_backgroundView - In the implementation block
@property (assign,nonatomic) double customInterItemSpacing;                           //@synthesize customInterItemSpacing=_customInterItemSpacing - In the implementation block
+(double)scaledFontSize:(double)arg1 withMaximumFontSizeCategory:(id)arg2 ;
// +(SCD_Struct_SB1)labelFontMetrics;
+(id)labelFont;
-(id)initWithString:(id)arg1 accessoryView:(id)arg2 ;
-(CGRect)subtitleLabelFrame;
-(double)baselineOffsetFromOrigin;
-(CGRect)backgroundViewFrame;
-(double)customInterItemSpacing;
-(void)setCustomInterItemSpacing:(double)arg1 ;
-(CGRect)accessoryViewFrame;
-(double)interItemSpacing;
-(id)init;
-(void)dealloc;
-(NSString *)string;
-(void)setFont:(UIFont *)arg1 ;
-(UIFont *)font;
-(void)layoutSubviews;
-(CGSize)sizeThatFits:(CGSize)arg1 ;
-(UIView *)backgroundView;
-(void)setAccessoryView:(UIView *)arg1 ;
-(UIView *)accessoryView;
-(void)setBackgroundView:(UIView *)arg1 ;
// -(_UILegibilitySettings *)legibilitySettings;
// -(void)setLegibilitySettings:(_UILegibilitySettings *)arg1 ;
-(void)setStrength:(double)arg1 ;
-(double)strength;
-(void)setString:(NSString *)arg1 ;
-(void)_updateForCurrentSizeCategory;
-(double)baselineOffsetFromBottom;
@end

@interface SBUILegibilityLabel : UIView
@property (nonatomic,copy) NSString *string;
@property (nonatomic,retain) UIFont *font;
-(void)setNumberOfLines:(long long)arg1;
-(void)setString:(NSString *)arg1;
-(void)setFrame:(CGRect)arg1;
@end

@interface WATodayPadView : UIView
@property (nonatomic,copy) UIImage * conditionsImage;
@property (nonatomic,copy) NSString * temperature;
@end
@interface WALockscreenWidgetViewController : UIViewController
-(WATodayPadView *)view;
@end

@interface SBFLockScreenDateView : UIView {
  SBUILegibilityLabel* _timeLabel;
}
@property (nonatomic, retain) NSNumber *timeStamp;
@property (nonatomic, retain) SBUICAPackageView *packageView;


@property (nonatomic) BOOL firstRun;
@property (nonatomic, retain) WATodayPadView *weatherWidget;
@property (nonatomic, retain) UIImageView *conditionsImageView;
@property (nonatomic, retain) UILabel *temperatureLabel;
@property (nonatomic) BOOL moved;
@property (assign,getter=isSubtitleHidden,nonatomic) BOOL subtitleHidden;
@property (assign,nonatomic) BOOL useCompactDateFormat;                                        //@synthesize useCompactDateFormat=_useCompactDateFormat - In the implementation block
@property (nonatomic,retain) NSDate * date;                                                    //@synthesize date=_date - In the implementation block
@property (nonatomic,retain) UIColor * textColor;                                              //@synthesize overrideTextColor=_overrideTextColor - In the implementation block
// @property (nonatomic,retain) _UILegibilitySettings * legibilitySettings;                       //@synthesize legibilitySettings=_legibilitySettings - In the implementation block
@property (assign,nonatomic) double timeLegibilityStrength;                                    //@synthesize timeLegibilityStrength=_timeLegibilityStrength - In the implementation block
@property (assign,nonatomic) double subtitleLegibilityStrength;                                //@synthesize subtitleLegibilityStrength=_subtitleLegibilityStrength - In the implementation block
@property (assign,nonatomic) double alignmentPercent;                                          //@synthesize alignmentPercent=_alignmentPercent - In the implementation block
@property (assign,nonatomic) double dateToTimeStretch;                                         //@synthesize dateToTimeStretch=_dateToTimeStretch - In the implementation block
@property (nonatomic,readonly) double timeBaselineOffsetFromOrigin;
@property (nonatomic,readonly) double subtitleBaselineOffsetFromOrigin;
@property (assign,nonatomic) CGRect restingFrame;                                              //@synthesize restingFrame=_restingFrame - In the implementation block
@property (nonatomic,readonly) double contentAlpha;
@property (nonatomic,retain) SBFLockScreenDateSubtitleView * customSubtitleView;               //@synthesize customSubtitleView=_customSubtitleView - In the implementation block
@property (readonly) unsigned long long hash;
@property (readonly) Class superclass;
@property (copy,readonly) NSString * description;
@property (copy,readonly) NSString * debugDescription;
@property (nonatomic,readonly) CGRect chargingVisualInformationTimeFrame;
@property (nonatomic,readonly) CGRect chargingVisualInformationTimeSubtitleFrame;
+(id)timeFont;
// +(SCD_Struct_SB1)timeFontMetrics;
+(double)defaultHeight;
-(void)setAlignmentPercent:(double)arg1 ;
-(void)setUseCompactDateFormat:(BOOL)arg1 ;
-(double)alignmentPercent;
-(BOOL)useCompactDateFormat;
-(void)updateFormat;
-(void)_setSubtitleAlpha:(double)arg1 ;
-(void)_updateUsesCompactDateFormat;
-(void)_updateLabelAlpha;
-(CGRect)_timeLabelFrameForAlignmentPercent:(double)arg1 ;
-(CGRect)_subtitleViewFrameForView:(id)arg1 alignmentPercent:(double)arg2 ;
-(UIEdgeInsets)_timeLabelInsetsForTimeString:(id)arg1 ;
-(UIEdgeInsets)_cachedGlyphInsetsTimeFontForString:(id)arg1 ;
-(BOOL)isSubtitleHidden;
-(void)setSubtitleHidden:(BOOL)arg1 ;
-(void)setTimeLegibilityStrength:(double)arg1 ;
-(void)setSubtitleLegibilityStrength:(double)arg1 ;
-(void)setDateToTimeStretch:(double)arg1 ;
-(void)setContentAlpha:(double)arg1 withSubtitleVisible:(BOOL)arg2 ;
-(void)setCustomSubtitleView:(SBFLockScreenDateSubtitleView *)arg1 ;
-(CGRect)presentationExtentForAlignmentPercent:(double)arg1 ;
-(double)timeBaselineOffsetFromOrigin;
-(double)subtitleBaselineOffsetFromOrigin;
-(double)dateToTimeStretch;
-(double)timeLegibilityStrength;
-(double)subtitleLegibilityStrength;
-(CGRect)restingFrame;
-(void)setRestingFrame:(CGRect)arg1 ;
-(SBFLockScreenDateSubtitleView *)customSubtitleView;
-(void)_updateLabels;
-(double)contentAlpha;
-(CGRect)chargingVisualInformationTimeFrame;
-(CGRect)chargingVisualInformationTimeSubtitleFrame;
-(id)_timeLabel;
-(id)initWithFrame:(CGRect)arg1 ;
-(void)layoutSubviews;
-(void)traitCollectionDidChange:(id)arg1 ;
-(NSDate *)date;
-(void)setTextColor:(UIColor *)arg1 ;
-(UIColor *)textColor;
// -(_UILegibilitySettings *)legibilitySettings;
// -(void)setLegibilitySettings:(_UILegibilitySettings *)arg1 ;
-(void)setDate:(NSDate *)arg1 ;
-(float)expectedLabelWidth:(SBUILegibilityLabel *)label;
@end
